package com.example.sumiz.kebuke;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

public class OrderPage extends AppCompatActivity {
    private String list_ite="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_page);
        //Spinner
        final Spinner item = (Spinner)findViewById(R.id.item);
        ArrayAdapter<CharSequence> itemm = ArrayAdapter.createFromResource(
                this, R.array.item, android.R.layout.simple_spinner_item );
        item.setAdapter(itemm);

        final Spinner ice = (Spinner) findViewById(R.id.ice);
        ArrayAdapter<CharSequence> icee = ArrayAdapter.createFromResource(
                this, R.array.ice, android.R.layout.simple_spinner_item );
        ice.setAdapter(icee);

        final Spinner sugar = (Spinner)findViewById(R.id.sugar);
        ArrayAdapter<CharSequence> sugarr = ArrayAdapter.createFromResource(
                this, R.array.sugar, android.R.layout.simple_spinner_item );
        sugar.setAdapter(sugarr);
        //spinner 值

        final TextView listdrk = (TextView)findViewById(R.id.list_drk);
        Button btn_add = (Button) findViewById(R.id.btn_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String ite = (String) item.getSelectedItem()+"  "+
                        (String) ice.getSelectedItem()+"  "+
                        (String) sugar.getSelectedItem();
                list_ite = list_ite + ite + "\n";
                listdrk.setText(list_ite);
            }
        });
        Button btn_clean = (Button) findViewById(R.id.btn_C);
        btn_clean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list_ite = "";
                listdrk.setText("");
            }
        });
        Button btn_next = (Button) findViewById(R.id.btn_next);
        btn_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setClass(OrderPage.this, CheckPage.class);
                startActivity(intent);
            }
        });
        Button btn_back = (Button) findViewById(R.id.btn_B);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(OrderPage.this)
                        .setTitle("取消定單")
                        .setMessage("您確定要取消訂單?")

                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                OrderPage.this.finish();
                            }
                        })
                        .setNegativeButton("取消",null)
                        .show();

            }
        });


    }
    public boolean onCreateOptionsMenu(Menu menu) {
        // 設置要用哪個menu檔做為選單
        getMenuInflater().inflate(R.menu.main_back, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_back) {
            OrderPage.this.finish();
        }
        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }


}
