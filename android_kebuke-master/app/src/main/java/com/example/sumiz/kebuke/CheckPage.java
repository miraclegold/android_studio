package com.example.sumiz.kebuke;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import java.security.cert.TrustAnchor;

public class CheckPage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_page);

        Button btn_back = (Button) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(CheckPage.this)
                        .setTitle("取消定單")
                        .setMessage("您確定要取消訂單?")

                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                Intent intent = new Intent();
                                intent.setClass(CheckPage.this, welcomePage.class);
                                startActivity(intent);
                                CheckPage.this.finish();

                            }
                        })
                        .setNegativeButton("取消",null)
                        .show();

            }
        });

        Button btn_send = (Button) findViewById(R.id.btn_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(CheckPage.this)
                        .setTitle("送出定單")
                        .setMessage("您確定要送出訂單?")

                        .setPositiveButton("確定", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {
                                Intent intent = new Intent();
                                intent.setClass(CheckPage.this, receiptPage.class);
                                startActivity(intent);
                            }
                        })
                        .setNegativeButton("取消",null)
                        .show();
            }
        });
    }


}
